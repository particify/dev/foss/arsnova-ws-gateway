package de.thm.arsnova.service.wsgateway.model

data class RoomSubscription(val subscriptionId: String, val roomId: String)
